package org.green.ipf.cassandra.client.main;

import org.green.ipf.cassandra.client.service.CassandraClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CassandraCleanMain implements CommandLineRunner {
    @Autowired
    private CassandraClientService cassandraClientService;

    @Override
    public void run(String... args) throws Exception {
        cassandraClientService.getVersion();
        cassandraClientService.dropKeyspace("ipf");
        cassandraClientService.dropKeyspace("ipf_cql");
        cassandraClientService.destroy();
    }

//    @Override
//    public void run(String... args) throws Exception {
//        Cluster cluster = null;
//        try {
//            cluster = Cluster.builder()
//                             .withoutJMXReporting()
//                             .addContactPoint("localhost")
//                             .build();
//            Session session = cluster.connect();
//
//            ResultSet rs = session.execute("select release_version from system.local");
//            Row row = rs.one();
//            System.out.println(row.getString("release_version"));
//
//            rs = session.execute("drop keyspace ipf;");
//
//            rs.one();
//
//            rs = session.execute("drop keyspace ipf;");
//
//        } finally {
//            if (cluster != null) cluster.close();
//        }
//    }
}
