package org.green.ipf.cassandra.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfCassandraRawClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfCassandraRawClientApplication.class, args);
	}

}
