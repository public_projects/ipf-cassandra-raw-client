package org.green.ipf.cassandra.client.service;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.InvalidConfigurationInQueryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CassandraClientService {
    private Logger logger = LoggerFactory.getLogger(CassandraClientService.class);

    @Autowired
    private Session session;

    @Autowired
    private Cluster cluster;

    public void getVersion() {
        ResultSet rs = session.execute("select release_version from system.local");    // (3)
        Row row = rs.one();
        logger.info(row.getString("release_version"));
    }

    public void dropKeyspace(String keyspace) {
        try {
            session.execute("drop keyspace "+keyspace+";");
            logger.info("dropped keyspace "+ keyspace);
        } catch (InvalidConfigurationInQueryException e) {
            logger.error(e.getMessage());
        }
    }

    public void destroy() {
        session.close();
        cluster.close();
        logger.info("Cassandra session closed!");
    }
}
