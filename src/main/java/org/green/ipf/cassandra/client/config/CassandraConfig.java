package org.green.ipf.cassandra.client.config;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CassandraConfig {

    @Bean(name = "cluster", destroyMethod = "close")
    public Cluster getCluster() {
        return Cluster.builder()
                      .withoutJMXReporting()
                      .addContactPoint("127.0.0.1")
                      .build();
    }

    @Bean(name = "session", destroyMethod = "close")
    public Session getSession(Cluster cluster) {
        return cluster.connect();
    }
}
